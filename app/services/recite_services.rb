class ReciteServices

  def initialize(params, lyrics)
    @params = params
    @lyrics = lyrics
  end

  def process
    get_number = @params[:number]
    count_lyric = @lyrics.size.to_i - 1
    count_temp = 0
    set_lyric = ""
    for i in 0..count_lyric
      count_temp = count_temp + 1

      if set_lyric == ""
        set_lyric = @lyrics[count_lyric - i]
      else
        set_lyric = @lyrics[count_lyric - i] + " " + set_lyric
      end
      
      break if count_temp.to_i == get_number.to_i
    end
    return set_lyric  
  end

  def process_2
    get_number = @params[:number]
    count_lyric = @lyrics.size.to_i - 1
    count_temp = 0
    set_lyric = ""
    for i in 0..count_lyric
      count_temp = count_temp + 1
      
      if set_lyric == ""
        set_lyric = get_the_subject_only(@lyrics[count_lyric - i])
      elsif count_temp.to_i == 2
        set_lyric = get_the_subject_only(@lyrics[count_lyric - i]) + " and " + set_lyric
      else
        set_lyric = get_the_subject_only(@lyrics[count_lyric - i]) + ", " + set_lyric
      end
      
      break if count_temp.to_i == get_number.to_i
    end
    return set_lyric  
  end

  private

  def get_the_subject_only(text)
    subject = text.split(" ")
    subject_return = subject[0] + " " + subject[1]
  end
end