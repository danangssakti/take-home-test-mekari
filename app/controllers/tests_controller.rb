class TestsController < ApplicationController
  before_action :set_constant_data
  before_action :validate_params, only: [:phase1, :phase2b] 

  def phase1
    result = ApplicationRecord::RESULT + ReciteServices.new(search_params, @constant_data).process
    render json: {data: result}
  end

  def phase2a
    result = ApplicationRecord::RESULT + ReciteServices.new(search_params_random(@constant_data.size), @constant_data).process
    render json: {data: result}
  end

  def phase2b
    result = ApplicationRecord::RESULT + ReciteServices.new(search_params, @constant_data).process_2
    render json: {data: result}
  end

  def phase2c
    result = ApplicationRecord::RESULT + ReciteServices.new(search_params_random(@constant_data.size), @constant_data).process_2
    render json: {data: result}
  end

  def method_name
    ganjil = ""
    genap = ""
    prima = ""

    for i in 1..15
      
      if i % 2 == 0
        if genap == ""
          genap = i.to_s
        else 
          genap = genap + ", " + i.to_s
        end
      end

      if i % 2 == 1
        if ganjil == ""
          ganjil = i.to_s
        else 
          ganjil = ganjil + ", " + i.to_s
        end
      end
      
      c = 0
      for j in 1..i
        if i % j = 0 
          c = c + 1
        end
      end
      if c == 2 
        if prima == ""
          prima = i.to_s
        else
          prima = prima + ", " + i.to_s
        end 
      end
    end
    render json: {ganjil: ganjil, genap: genap, prima: prima}
  end

  private

  def set_constant_data
    @constant_data = ApplicationRecord::LYRICS
  end

  def search_params
    {
      number: params[:number]
    }
  end

  def search_params_random(count_lyrics)
    number_random = rand(0..(count_lyrics-1))
    {
      number: number_random 
    }
  end

  def validate_params
    return render_error_400("params number is required") unless params[:number].present?
  end
end