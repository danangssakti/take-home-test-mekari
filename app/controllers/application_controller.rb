class ApplicationController < ActionController::API

  def render_error_400(detail = 'Bad Request')
    render json: {
      title: 'Bad Request',
      detail: detail,
      status_code: 400
    },
    status: 400
  end

end
