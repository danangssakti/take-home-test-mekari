Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get "/test-phase-1", to: "tests#phase1"
  get "/test-phase-2-a", to: "tests#phase2a"
  get "/test-phase-2-b", to: "tests#phase2b"
  get "/test-phase-2-c", to: "tests#phase2c"
end
