require 'rails_helper'

RSpec.describe "GET /test-phase-2-c", type: :request do

  it "succes, will show result", :aggregate_failures  do
    get "/test-phase-2-c"
    body = JSON.parse(response.body)
    result = body["data"].downcase.split("this is ")
    expect(response).to have_http_status :ok
    expect("the horse, the farmer, the rooster, the priest, the man, the maiden, the cow, the dog, the cat, the rat, the malt and the house").to include(result[1].to_s)
  end

end