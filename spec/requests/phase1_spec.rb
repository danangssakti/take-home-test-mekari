require 'rails_helper'

RSpec.describe "GET /test-phase-1", type: :request do

  it "succes, will show result for params number 1", :aggregate_failures  do
    get "/test-phase-1", params: {number: 1}
    body = JSON.parse(response.body)
    expect(response).to have_http_status :ok
    expect(body["data"].downcase).to eq ("this is the house that jack built")
  end

  it "succes, will show result for params number 12", :aggregate_failures  do
    get "/test-phase-1", params: {number: 12}
    body = JSON.parse(response.body)
    expect(response).to have_http_status :ok
    expect(body["data"].downcase).to eq ("this is the horse and the hound and the horn that belonged to the farmer sowing his corn that kept the rooster that crowed in the morn that woke the priest all shaven and shorn that married the man all tattered and torn that kissed the maiden all forlorn that milked the cow with the crumpled horn that tossed the dog that worried the cat that killed the rat that ate the malt that lay in the house that jack built")
  end

  it "fail, will show bad request message if no value for params[:number]" do
    get "/test-phase-1"
    body = JSON.parse(response.body)
    expect(response).to have_http_status (400)
  end
end