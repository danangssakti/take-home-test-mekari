require 'rails_helper'

RSpec.describe "GET /test-phase-2-b", type: :request do

  it "succes, will show result", :aggregate_failures  do
    get "/test-phase-2-b", params: {number: 1}
    body = JSON.parse(response.body)
    expect(response).to have_http_status :ok
    expect(body["data"].downcase).to eq ("this is the house")
  end

  it "succes, will show result", :aggregate_failures  do
    get "/test-phase-2-b", params: {number: 2}
    body = JSON.parse(response.body)
    expect(response).to have_http_status :ok
    expect(body["data"].downcase).to eq ("this is the malt and the house")
  end

  it "succes, will show result", :aggregate_failures  do
    get "/test-phase-2-b", params: {number: 3}
    body = JSON.parse(response.body)
    expect(response).to have_http_status :ok
    expect(body["data"].downcase).to eq ("this is the rat, the malt and the house")
  end

  it "succes, will show result", :aggregate_failures  do
    get "/test-phase-2-b", params: {number: 5}
    body = JSON.parse(response.body)
    expect(response).to have_http_status :ok
    expect(body["data"].downcase).to eq ("this is the dog, the cat, the rat, the malt and the house")
  end

  it "fail, will show bad request message if no value for params[:number]" do
    get "/test-phase-2-b"
    body = JSON.parse(response.body)
    expect(response).to have_http_status (400)
  end
end