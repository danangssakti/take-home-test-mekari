require 'rails_helper'

RSpec.describe "GET /test-phase-2-a", type: :request do

  it "succes, will show result", :aggregate_failures  do
    get "/test-phase-2-a"
    body = JSON.parse(response.body)
    result = body["data"].downcase.split("this is ")
    expect(response).to have_http_status :ok
    expect("the horse and the hound and the horn that belonged to the farmer sowing his corn that kept the rooster that crowed in the morn that woke the priest all shaven and shorn that married the man all tattered and torn that kissed the maiden all forlorn that milked the cow with the crumpled horn that tossed the dog that worried the cat that killed the rat that ate the malt that lay in the house that jack built").to include(result[1].to_s)
  end

end